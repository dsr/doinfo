Get info from your Digital Ocean droplets
=========================================

I hope to make this an extension of what you can do with their API. There are some things, like getting disk usage, that can't be done with the API.

Instructions
------------

Add your info to config_example.py and rename to config.py. Do *NOT* commit config.py to source control with your confidential information in it!
